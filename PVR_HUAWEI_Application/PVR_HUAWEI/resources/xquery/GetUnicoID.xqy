xquery version "2004-draft" encoding "Cp1252";

declare namespace xf = "http://tempuri.org/PVR_HUAWEI/resources/xquery/GetUnicoID/";

declare function xf:DateFormatStr($originalFormat as xs:string,$DateToFormat as xs:string,$returnFormat as xs:string)
		as xs:string {
		   fn-bea:dateTime-to-string-with-format($returnFormat, fn-bea:dateTime-from-string-with-format($originalFormat, $DateToFormat))
};
	
declare function xf:GetRandomCincoDigitos($uudiIn as xs:string) as xs:string{
	let $uuid_5	:= substring(replace($uudiIn, '-', ''), 1, 5)
	return $uuid_5
};

declare function xf_ValidateMessageSeq($messageSeqIn as xs:string) as xs:string{
 
let $messageSeq	:= if(string-length($messageSeqIn) <= 64) then $messageSeqIn   
					else substring($messageSeqIn, 1, 64) 
	return $messageSeq
};

declare function xf:getMessageSeq($messageSeqIn as xs:string?, $accessModeIn as xs:string?, $uuidIn as xs:string)
    as xs:string {
    	let $idSistema			:= 'MDW'
    	let $accessMode			:= if(fn:not(fn:string-length($accessModeIn)>0)) then 'X' else $accessModeIn
    	let $fechaDeSistema		:= xf:DateFormatStr("yyyy-MM-dd'T'HH:mm:ss",fn:substring(xs:string(fn:current-dateTime()),1,19),'yyyyMMddHHmmss') 
    	let $numeroAleatorio	:= xf:GetRandomCincoDigitos(xs:string($uuidIn))
    	let $uniqueID  			:= fn:concat($messageSeqIn, '_', $fechaDeSistema, '_', $numeroAleatorio, '_', $accessMode, '_', $idSistema)
    	let $validateUniqueID	:= xf_ValidateMessageSeq($uniqueID)
    	return $validateUniqueID
};

declare function xf:GetUnicoID($messageSeqIn as xs:string?,
    $accessModeIn as xs:string?,
    $uuidIn as xs:string)
    as xs:string {
        xs:string(xf:getMessageSeq($messageSeqIn, $accessModeIn, $uuidIn))
};

declare variable $messageSeqIn as xs:string? external;
declare variable $accessModeIn as xs:string? external;
declare variable $uuidIn as xs:string external;

xf:GetUnicoID($messageSeqIn,
    $accessModeIn,
    $uuidIn)