xquery version "2004-draft";
(:: pragma bea:schema-type-parameter parameter="$requestHeader" type="ns0:RequestHeader" location="../../CBSInterface_MessageHeader.xsd" ::)
(:: pragma bea:schema-type-return type="ns0:RequestHeader" location="../../CBSInterface_MessageHeader.xsd" ::)

declare namespace ns0 = "http://www.huawei.com/bme/cbsinterface/cbscommon";
declare namespace xf = "http://tempuri.org/PVR_HUAWEI/resources/xquery/OCSRequestHeader/";


declare function xf:OCSRequestHeader($requestHeader as element()?,
    $usuario as xs:string,
    $password as xs:string,
    $accesMode as xs:string,
    $version as xs:string,
    $uudi as xs:string)
    as element() {
        <RequestHeader>
            <ns0:Version>{ $version }</ns0:Version>
            {
                for $BusinessCode in $requestHeader/ns0:BusinessCode
                return
                    <ns0:BusinessCode>{ data($BusinessCode) }</ns0:BusinessCode>
            }
            <ns0:MessageSeq>{ data($uudi) }</ns0:MessageSeq>
            {
                for $OwnershipInfo in $requestHeader/ns0:OwnershipInfo
                return
                    <ns0:OwnershipInfo>{ $OwnershipInfo/@* , $OwnershipInfo/node() }</ns0:OwnershipInfo>
            }
            <ns0:AccessSecurity>
                <ns0:LoginSystemCode>{ $usuario }</ns0:LoginSystemCode>
                <ns0:Password>{ $password }</ns0:Password>
            </ns0:AccessSecurity>
            {
                for $OperatorInfo in $requestHeader/ns0:OperatorInfo
                return
                    <ns0:OperatorInfo>{ $OperatorInfo/@* , $OperatorInfo/node() }</ns0:OperatorInfo>
            }
            {
              if(fn:not(fn:exists($requestHeader))) then
                <ns0:AccessMode>{ $accesMode }</ns0:AccessMode> (:default si no me mandaron el header:)
              else
                if(fn:exists($requestHeader/ns0:AccessMode)) then
                    <ns0:AccessMode>{ $accesMode }</ns0:AccessMode> (:default si me mandaron el header, obedezco al header:)
                else()
            }
            {
                for $MsgLanguageCode in $requestHeader/ns0:MsgLanguageCode
                return
                    <ns0:MsgLanguageCode>{ data($MsgLanguageCode) }</ns0:MsgLanguageCode>
            }
            {
                for $TimeFormat in $requestHeader/ns0:TimeFormat
                return
                    <ns0:TimeFormat>{ $TimeFormat/@* , $TimeFormat/node() }</ns0:TimeFormat>
            }
            {
                for $AdditionalProperty in $requestHeader/ns0:AdditionalProperty
                return
                    <ns0:AdditionalProperty>{ $AdditionalProperty/@* , $AdditionalProperty/node() }</ns0:AdditionalProperty>
            }
        </RequestHeader>
};

declare variable $requestHeader as element()? external;
declare variable $usuario as xs:string external;
declare variable $password as xs:string external;
declare variable $accesMode as xs:string external;
declare variable $version as xs:string external;
declare variable $uudi as xs:string external;


xf:OCSRequestHeader($requestHeader,
    $usuario,
    $password,
    $accesMode,
    $version,
    $uudi)
