xquery version "2004-draft";
(:: pragma bea:global-element-return element="ns0:ConsultarSuspensionResponseSOAP" location="../xsd/ConsultaSuspension.xsd" ::)

declare namespace ns2 = "http://www.personal.com.ar/Common/HeaderResponse/v1.0";
declare namespace ns1 = "http://www.personal.com.ar/Common/Entities/Cliente/Suspension/v1.0";
declare namespace ns0 = "http://www.personal.com.ar/ESB/SuspensionesOCS/v1.0";
declare namespace xf = "http://tempuri.org/PVR_HUAWEI/resources/xquery/SuspensionesResponse/";


declare function xf:isMarcaSuspensionActiva($statusDetail as xs:string, $digitoNoHabilitado as xs:integer)
    as xs:boolean {
    	let $statusDetailAfter  	:= substring($statusDetail , ($digitoNoHabilitado + 1))
    	let $statusDetailBefore  	:= substring($statusDetail , 1, ($digitoNoHabilitado - 1))
		let $nuevoStatusDetail		:= concat($statusDetailBefore, $statusDetailAfter)
		let $isCero := matches($nuevoStatusDetail, "[1|2|3|4|5|6|7|8|9]")
    	return $isCero
};

declare function xf:marcaSuspensionActiva($statusDetail as xs:string, $position as xs:integer)
    as xs:boolean {
    	
    	let $digitPosition := xs:integer(substring($statusDetail, $position, 1))
		let $marcaSuspensionActiva := if($digitPosition = 0) then false()
		else true()
		return $marcaSuspensionActiva
};

declare function xf:tipoSuspension($statusDetail as xs:string, $position as xs:integer)
    as xs:string {
		let $digitPosition := xs:integer(substring($statusDetail, $position, 1))
		let $tipoSuspension := if($digitPosition = 1) then 'PARCIAL'
		else 'TOTAL'
		return $tipoSuspension
};



declare function xf:SuspensionesResponse($StatusDetailSTR as xs:string, $suspensionesSTR as xs:string)
    as element(ns0:ConsultarSuspensionResponseSOAP) {
    
        <ns0:ConsultarSuspensionResponseSOAP>
            <ns0:Body>
                <ns0:ConsultarSuspension>
                
                	<ns1:marcaSuspensionActiva>{xf:isMarcaSuspensionActiva($StatusDetailSTR, 5)}</ns1:marcaSuspensionActiva>
                	
                    {
                    
                    if (xf:isMarcaSuspensionActiva($StatusDetailSTR, 5) = true()) then
                     
                    <ns0:ListaSuspensiones>
                    {	
                    
                 	for $suspension at $pos in tokenize($suspensionesSTR, '[,\s]+') 
    				let $digitPosition := xs:integer(substring($StatusDetailSTR, $pos, 1))
    				
    				return
    				if (($digitPosition != 0) and ($pos != 5)) then
    				<ns0:Suspension> 
                            <ns1:codSuspension>{$suspension}</ns1:codSuspension>
							<ns1:marcaSuspensionActiva>{xf:marcaSuspensionActiva($StatusDetailSTR, $pos)}</ns1:marcaSuspensionActiva>
							<ns1:tipoSuspension>{xf:tipoSuspension($StatusDetailSTR, $pos)}</ns1:tipoSuspension>
					</ns0:Suspension>
					else ()
               		}
                    </ns0:ListaSuspensiones>
                else()
                
                }
                </ns0:ConsultarSuspension>
            </ns0:Body>
        </ns0:ConsultarSuspensionResponseSOAP>
};

declare variable $StatusDetailSTR as xs:string external;
declare variable $suspensionesSTR as xs:string external;

xf:SuspensionesResponse($StatusDetailSTR, $suspensionesSTR)